from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime,Float,CheckConstraint
from sqlalchemy.orm import relationship

from app.database import Base


#-----------------------------------------------------------------------------------
#  USER
#-----------------------------------------------------------------------------------

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    balance = Column(Float,default=0.0)
    
    transactions = relationship("Transaction",back_populates="owner")
    orders = relationship("Order",back_populates="owner")



#----------------------------------------------------------------------------------
#  TRANSACTIONS
#----------------------------------------------------------------------------------
class Transaction(Base):
    __tablename__ = "transactions"
    id = Column(Integer,primary_key=True)
    trans_type = Column(String,nullable=False)
    trans_timestamp = Column(DateTime,nullable=False)
    amount = Column(Integer,nullable=False)
    user_id = Column(Integer,ForeignKey("users.id"))
    owner = relationship("User",back_populates="transactions")


    __table_args__ =(CheckConstraint(trans_type.in_(['withdraw','deposit'])),)

#----------------------------------------------------------------------------------
#  ORDERS
#----------------------------------------------------------------------------------
class Order(Base):
    __tablename__ = "orders"
    id = Column(Integer,primary_key=True)
    order_type = Column(String,nullable=False)
    order_timestamp = Column(String,nullable=False)
    order_status = Column(String,nullable=False)
    order_note = Column(String,nullable=False)
    total = Column(Integer)
    upper_bound = Column(Integer)
    lower_bound = Column(Integer)
    user_id = Column(Integer,ForeignKey("users.id"))
    stock_id = Column(String,ForeignKey("stocks.id"))

    stock = relationship("Stock",back_populates="users")
    owner = relationship("User",back_populates="orders")

    __table_args__=(CheckConstraint(order_type.in_(['buy','sell'])),CheckConstraint(order_status.in_(['rejected','accepted'])))