from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime,Float,CheckConstraint
from sqlalchemy.orm import relationship

from app.database import Base
#----------------------------------------------------------------------------------
#  STOCKS
#----------------------------------------------------------------------------------

class Stock(Base):
    __tablename__ ="stocks"
    id = Column(String,primary_key=True)
    name = Column(String,nullable=False)
    queue = relationship("StockQueue")
    users = relationship("Order",back_populates="stock")
    

class StockQueue(Base):
    __tablename__ = "stock_queue"
    id = Column(Integer,primary_key=True)
    stock_id = Column(String,ForeignKey("stocks.id"))
    availability = Column(Integer,nullable=False)
    timestamp = Column(DateTime,nullable=False)
    price = Column(Integer,nullable=False)
    stock = relationship("Stock",back_populates="queue")