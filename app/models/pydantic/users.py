from typing import List, Optional

from pydantic import BaseModel
from datetime import datetime
from app.models.pydantic.transactions import Transaction
from app.models.pydantic.orders import Order
#-----------------------------------------------------------------------------------
#  USER
#-----------------------------------------------------------------------------------

class UserBase(BaseModel):
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    balance: float
    transactions: List[Transaction] = []
    orders: List[Order]=[]
    class Config:
        orm_mode = True
