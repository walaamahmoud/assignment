from typing import List, Optional

from pydantic import BaseModel
from datetime import datetime

#----------------------------------------------------------------------------------
# TRANSACTIONS
#----------------------------------------------------------------------------------

class TransactionBase(BaseModel):
    user_id: int
    amount: int


class TransactionCreate(TransactionBase):
    pass

class Transaction(TransactionBase):
    trans_timestamp: datetime
    trans_type: str

    class Config:
        orm_mode= True 