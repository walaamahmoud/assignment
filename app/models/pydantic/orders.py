from typing import List, Optional

from pydantic import BaseModel
from datetime import datetime
#-----------------------------------------------------------------------------------
#ORDER
#-----------------------------------------------------------------------------------
class OrderBase(BaseModel):
    user_id: int
    stock_id: str
    upper_bound: int
    lower_bound: int
    total: int

class OrderCreate(OrderBase):
    pass 

class Order(OrderBase):
    order_timestamp: datetime
    order_type: str 
    order_note: str 
    order_status: str
    class Config:
        orm_mode = True