from typing import List, Optional

from pydantic import BaseModel
from datetime import datetime

#----------------------------------------------------------------------------------
# STOCKS
#----------------------------------------------------------------------------------


class StockQueueBase(BaseModel):
    # title: str
    # description: Optional[str] = None
    stock_id: str 
    price: int 
    availability: int 
    timestamp: datetime 
    
    

class StockQueueCreate(StockQueueBase):
    pass


class StockQueue(StockQueueBase):
    

    class Config:
        orm_mode = True



class StockBase(BaseModel):
    id: str
    name: str 

class StockCreate(StockBase):
    pass 

class Stock(StockBase):
    id: str 
    name: str 
    queue: List[StockQueue]=[]


    class Config:
        orm_mode= True