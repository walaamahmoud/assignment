from typing import List
from fastapi import Depends, FastAPI, HTTPException, Request, Response
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse
from sqlalchemy.orm import Session
from app.database import SessionLocal, engine,session
from datetime import datetime
import logging
import paho.mqtt.client as mqtt
import logging
import time
from app import routers

app = FastAPI()


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return PlainTextResponse(str(exc), status_code=400)

------------------------------------------------------------------------------------
CONSUMER
------------------------------------------------------------------------------------


def consume(msg):
    try:
        exists = crud.get_stock_by_id(session,msg['stock_id'])
        if exists:
             pass
        else:
           
            try:
                db_stock = models.Stock(id =msg["stock_id"],name=msg['name'])
                session.add(db_stock)
                session.commit()
                session.refresh(db_stock)
                print("\u001b[32m"+"NEW STOCK IS ADDED")
            
            except Exception as e:
                print("\u001b[31m"+"STOCK ERROR:",e)
    except Exception as e:
        print(e)
    
    try:
        db_queue = models.StockQueue(stock_id =msg['stock_id'],availability=msg['availability'],timestamp= msg['timestamp'],price=msg['price'])
        session.add(db_queue)
        session.commit()
        session.refresh(db_queue)
        print("\u001b[32m"+"NEW RECORD IS ADDED TO STOCK QUEUE")
    except Exception as e:
        print("\u001b[31m"+"QUEUE ERROR:",e)

    


def on_connect(client, userdata, flags,rc):
    print("Connected with result code "+str(rc))

    client.subscribe("thndr-trading")

def on_message(client, userdata, msg):

    print("\u001b[32m"+"on_message is called")
    msg = eval(msg.payload)   
    consume(msg)
    




@app.on_event("startup")
async def startup():

    logging.info("\u001b[36m"+"Starting UP")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    print("TRYING TO CONNECT")
    time.sleep(10)

    client.connect("vernemq", 1883, 60)
    client.loop_start()


@app.on_event("shutdown")
async def shutdown():
    logging.info("\u001b[36m"+"Shutting down")
   


app.include_router(
    routers.users,prefix='/users',tags=["users"]
)

app.include_router(
    routers.transactions,prefix='/transactions',tags=["transactions"]
)

app.include_router(
    routers.orders,prefix='/orders',tags=["orders"]
)

app.include_router(
    routers.stocks,prefix='/stocks',tags=["orders"]
)