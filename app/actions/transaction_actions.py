from sqlalchemy.orm import Session
from sqlalchemy import case,func,and_
from app.models.db.db import User as user_table
from app.models.db.db import Transaction as transaction_table
from app.actions import user_actions
from app.models.pydantic.transactions import Transaction,TransactionCreate
from datetime import datetime
from fastapi import  HTTPException
#----------------------------------------------------------------------------------
# TRANSACTIONS
#----------------------------------------------------------------------------------

def create_user_transaction(db: Session, transaction: TransactionCreate,trans_type: str):
    
    db_trans = transaction_table(**transaction.dict(),trans_type= trans_type,trans_timestamp= datetime.now())
    db.add(db_trans)
    db.commit()
    db.refresh(db_trans)
    return db_trans


def withdraw(db:Session,transaction:TransactionCreate):
    db_user = user_actions.get_user(db, user_id=transaction.user_id)

    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    current_user_balance =  user_actions.get_user_balance(db=db,user_id=transaction.user_id)[0]
    if current_user_balance < transaction.amount:
        raise HTTPException(status_code=400, detail="User balance is insufficient")
    user_actions.update_user_balance(db=db,user_id=transaction.user_id,amount = current_user_balance-transaction.amount)
    return create_user_transaction(db,transaction,'withdraw')

def deposit(db:Session,transaction:TransactionCreate):
    db_user = user_actions.get_user(db, user_id=transaction.user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    current_user_balance =  user_actions.get_user_balance(db=db,user_id=transaction.user_id)[0]

    user_actions.update_user_balance(db=db,user_id=transaction.user_id,amount = transaction.amount +current_user_balance)


    return create_user_transaction(db,transaction,'deposit')
