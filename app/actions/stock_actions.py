from sqlalchemy.orm import Session
from sqlalchemy import case,func,and_
# from app import models, schemas
from fastapi import HTTPException
from app.models.db.stocks import StockQueue as stock_queue_table 
from app.models.db.stocks import Stock as stock_table 
from app.models.db.db import Order as order_table
from app.models.pydantic.stocks import StockCreate,StockQueueCreate,Stock,StockQueue
from datetime import datetime
#----------------------------------------------------------------------------------
# STOCKS
#----------------------------------------------------------------------------------

def create_stock_queue(db: Session, queue: StockQueueCreate):
    db_queue = stock_queue_table(**queue.dict())
    db.add(db_queue)
    db.commit()
    db.refresh(db_queue)
    return db_queue


def create_stock(db:Session,stock: StockCreate):
    db_stock = stock_table(**stock.dict())
    db.add(db_stock)
    db.commit()
    db.refresh(db_stock)
    return db_stock

def get_stock_by_id(db: Session, id: str):
    return db.query(stock_table).filter(stock_table.id == id).first()


def get_stock_availability_and_price(db:Session,stock_id: str):
    stock_info = db.query(stock_queue_table).filter(stock_queue_table.stock_id == stock_id).order_by(stock_queue_table.timestamp.desc()).first()

    return stock_info.availability,stock_info.price

def get_stock_per_user(db:Session,user_id: int, stock_id:str):
    '''
    SELECT sum(case when order_type='buy' then total end) -sum(case when order_type='sell' then total end) user_stock_total,
    from orders
    where user_id = {user_id}
    and stock_id = {stock_id}
    and status= 'accepted' 

    '''
    bought_stocks = db.query(
                            func.sum(case(whens={order_table.order_type=="buy":order_table.total})),
                            func.sum(case(whens={order_table.order_type=="sell":order_table.total}))
                            ).filter(
                                and_(order_table.order_status=="accepted"
                                ,order_table.user_id==user_id
                                ,order_table.stock_id==stock_id)).first()
    bought = bought_stocks[0] if bought_stocks[0] is not None else 0
    sold = bought_stocks[1] if bought_stocks[1] is not None else 0
    
    return bought - sold