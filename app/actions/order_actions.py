from sqlalchemy.orm import Session
from sqlalchemy import case,func,and_
from app.models.db.db import Order as order_table
from app.models.pydantic.orders import Order,OrderCreate
from datetime import datetime
from app.actions import stock_actions,user_actions
from fastapi import HTTPException

#----------------------------------------------------------------------------------
# ORDERS
#----------------------------------------------------------------------------------

def create_order_for_user(db:Session,order:OrderCreate,order_type: str, order_note: str,order_status: str):
    db_order = order_table(**order.dict(),order_timestamp=datetime.now(),order_type=order_type,order_note=order_note,order_status=order_status)
    db.add(db_order)
    db.commit()
    db.refresh(db_order)
    return db_order


  

def buy_stock(db:Session,order:OrderCreate):
    db_user = user_actions.get_user(db, user_id=order.user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    db_stock = stock_actions.get_stock_by_id(db, id=order.stock_id)
    if db_stock is None:
        raise HTTPException(status_code=404, detail="Stock not found")

    current_user_balance = user_actions.get_user_balance(db=db,user_id= order.user_id)[0]

    availability,price = stock_actions.get_stock_availability_and_price(db=db,stock_id=order.stock_id) 
    
    order_price = order.total * price 
    order_price = order.total * price 

    if price> order.upper_bound or price < order.lower_bound:
        raise HTTPException(status_code=400, detail="stock price is out of bounds") 
    elif availability < order.total:
        raise HTTPException(status_code=400, detail="stock is not available")
    elif order_price > current_user_balance:
        raise HTTPException(status_code=400, detail="user balance is not sufficient")
    else:

        user_actions.update_user_balance(db=db,user_id=order.user_id,amount= current_user_balance-order_price)
        return create_order_for_user(db=db,order= order,order_type='buy',order_note="successful order",order_status="accepted")

def sell_stock(db:Session,order:OrderCreate):
    db_user = user_actions.get_user(db, user_id=order.user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    db_stock = stock_actions.get_stock_by_id(db, id=order.stock_id)
    if db_stock is None:
        raise HTTPException(status_code=404, detail="Stock not found")

    total_stocks_owned_by_user = stock_actions.get_stock_per_user(db=db,user_id=order.user_id,stock_id=order.stock_id)
    availability,price = stock_actions.get_stock_availability_and_price(db=db,stock_id=order.stock_id) 

    if order.total > total_stocks_owned_by_user:
        raise HTTPException(status_code=400, detail="stock is not available for user")

    elif price> order.upper_bound or price < order.lower_bound:
        raise HTTPException(status_code=400, detail="stock price is out of bounds")

    else:
        current_user_balance = user_actions.get_user_balance(db=db,user_id= order.user_id)[0]
        order_price = price * order.total
        user_actions.update_user_balance(db=db,user_id=order.user_id,amount= current_user_balance+order_price)
        return create_order_for_user(db=db,order= order,order_type='sell',order_note="successful order",order_status="accepted")
