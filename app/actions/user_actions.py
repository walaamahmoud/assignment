from sqlalchemy.orm import Session
from sqlalchemy import case,func,and_
# from app import models, schemas
from app.models.db.db import User as user_table
from app.models.pydantic.users import UserCreate, User
from datetime import datetime
#-----------------------------------------------------------------------------------
#  USER
#-----------------------------------------------------------------------------------

def get_user(db: Session, user_id: int):
    return db.query(user_table).filter(user_table.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(user_table).filter(user_table.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(user_table).offset(skip).limit(limit).all()


def create_user(db: Session, user: UserCreate):
    fake_hashed_password = user.password + "notreallyhashed"
    db_user = user_table(email=user.email, hashed_password=fake_hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def get_user_balance(db:Session,user_id: int):
    return db.query(user_table.balance).filter(user_table.id==user_id).first()

def update_user_balance(db:Session,user_id: int ,amount:float):
    
    db.query(user_table).filter(user_table.id==user_id).update({"balance":amount})
    db.commit()
    return True