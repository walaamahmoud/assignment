from databases import Database
from sqlalchemy import MetaData, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from app import settings

metadata = MetaData()

db = Database(settings.DATABASE_URI)
db_sync_engine = create_engine(settings.DATABASE_URI)

engine = create_engine(
    settings.DATABASE_URI#, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
metadata = MetaData()
session = SessionLocal()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
