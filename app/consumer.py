
import paho.mqtt.client as mqtt
import time
import logging
import crud



def create_record(msg):
    result = session.query(Stock).filter(Stock.id==msg['stock_id']) 
    if result.first()is None:
        new_stock = Stock(id = msg['stock_id'],name=msg['name'])
        session.add(new_stock)
        print("\u001b[32m" +"NEW RECORD IS ADDED TO STOCKS")
        session.commit()

    new_queue_record = StockQueue(stock_id = msg['stock_id'],price = msg['price'],availability= msg['availability'],timestamp=msg['timestamp'])

    session.add(new_queue_record)
    print("\u001b[32m" +"NEW RECORD IS ADDED TO THE QUEUE")
    session.commit()


def on_connect(client, userdata, flags,rc):
    print("Connected with result code "+str(rc))

    client.subscribe("thndr-trading")

def on_message(client, userdata, msg):

    print("\u001b[32m"+"on_message is called")
    create_record(eval(msg.payload))


if __name__=="__main__":
    # client = mqtt.Client()
    # client.on_connect = on_connect
    # client.on_message = on_message
    # print("TRYING TO CONNECT")
    # time.sleep(10)

    # client.connect("vernemq", 1883, 60)
    # client.loop_forever()
    msg = {"stock_id": "f9efc6e6-0b7d-4671-bb6d-2e4c51e40337", "name": "Hamada Inc", "price": 95, "availability": 101, "timestamp": "2020-08-19 14:44:23.783004"}
    create_record(queue,msg,db=Depends(get_db))