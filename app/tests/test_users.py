import pytest 
import json
# -------------------------------------------------------
# USER ROUTES TESTING 
# -------------------------------------------------------
def test_get_user_info(client):
    url = '/users'
    response = client.get(url,json={"user_id":1})
    assert response.status_code == 200
    response = client.get(url+'/3')
    assert response.status_code == 404 


