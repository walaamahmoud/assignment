import pytest

def test_accepted_buy_order(client):
    url = '/orders/buy'
    response = client.post(
        url,
        json={"stock_id":"6ffb8e62-92c1-40c7-9d38-5b976a346b62",
              "user_id":1,
              "upper_bound":60,
              "lower_bound":20,
              "total":1})
    assert response.status_code==201
    assert response.json()['order_status']=='accepted'
    assert response.json()['order_type']=='buy'
def test_rejected_buy_order(client):
    url = '/orders/buy'
    stock_id = '8ce050c8-53c6-48e6-a4b0-bafbe1ae26b9'
    # test insufficient balance
    response = client.post(
        url,
        json={"stock_id":stock_id,
              "user_id":1,
              "upper_bound":1000,
              "lower_bound":0,
              "total":80})
    print(response.json())
    print(response.status_code)
    assert response.status_code==400
    assert response.json()['detail']=="user balance is not sufficient"

     # test out of bounds order
    response = client.post(
        url,
        json={"stock_id":stock_id,
              "user_id":1,
              "upper_bound":0,
              "lower_bound":0,
              "total":1})
    assert response.status_code==400

    assert response.json()['detail']=="stock price is out of bounds"
     # test stock availability 
    response = client.post(
        url,
        json={"stock_id":stock_id,
              "user_id":1,
              "upper_bound":1000,
              "lower_bound":0,
              "total":10000})
    assert response.status_code==400
    assert response.json()['detail']=="stock is not available"

def test_accepted_sell_order(client):
    url = '/orders/sell'
    response = client.post(
        url,
        json={"stock_id":"6ffb8e62-92c1-40c7-9d38-5b976a346b62",
              "user_id":1,
              "upper_bound":60,
              "lower_bound":20,
              "total":1})
    assert response.status_code==201
    assert response.json()['order_status']=='accepted'
    assert response.json()['order_type']=='sell'


def test_rejected_sell_order(client):
    url = '/orders/sell'
    stock_id = '8ce050c8-53c6-48e6-a4b0-bafbe1ae26b9'
    response = client.post(
        url,
        json={"stock_id":stock_id,
              "user_id":1,
              "upper_bound":1000,
              "lower_bound":0,
              "total":80})
    print(response.json())
    print(response.status_code)
    assert response.status_code==400
    assert response.json()['detail']=="stock is not available for user"

     # test out of bounds order
    response = client.post(
        url,
        json={"stock_id":"6ffb8e62-92c1-40c7-9d38-5b976a346b62",
              "user_id":1,
              "upper_bound":0,
              "lower_bound":0,
              "total":1})
    assert response.status_code==400

    assert response.json()['detail']=="stock price is out of bounds"
  
