import pytest 
import json
# -------------------------------------------------------
# TRANSACTION TESTING
# -------------------------------------------------------
def test_accepted_deposit(client):
    url ="/transactions/deposit"

    response = client.post(
        url,
        json={"user_id":1,"amount":50.5}

    )
    response_data = json.loads(response.text)
    print(response_data)
    assert response_data['amount'] == 50
    assert response.status_code == 202


    params = {"user_id":1,"amount":100}
    response = client.post(url,json=params)

    response_data = json.loads(response.text)

    assert response_data["amount"]== params["amount"]
    assert response.status_code == 202


def test_rejected_deposit(client):
   
    url ="transactions/deposit"

    response = client.post(
        url,
        json={"user_id":3,"amount":50}

    )
    assert response.status_code == 404
    response = client.post(
        url,
        json={"user_id":1,"amount":"amount"}

    )
    assert response.status_code == 400



def test_accepted_withdrawal(client):
    url ="transactions/withdraw"

    response = client.post(
        url,
        json={"user_id":1,"amount":50.5}

    )
    response_data = json.loads(response.text)
    assert response_data['amount']==50
    assert response.status_code == 202
    response = client.post(
        url,
        json={"user_id":1,"amount":50}

    )
    response_data = json.loads(response.text)
    assert response.json()['amount']==50
    assert response.status_code == 202

def test_rejected_withdrawal(client):
    url ="transactions/withdraw"

    response = client.post(
        url,
        json={"user_id":1,"amount":"amount"}

    )
    assert response.status_code == 400
    response = client.post(
        url,
        json={"user_id":3,"amount":50}

    )
    assert response.status_code == 404
    response = client.post(
        url,
        json={"user_id":1,"amount":8000}

    )
    assert response.status_code == 400
     