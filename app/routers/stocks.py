from typing import List

from fastapi import Depends, FastAPI, HTTPException, Request, Response, status, APIRouter
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse

from sqlalchemy.orm import Session

from app.actions import stock_actions
from app.models.pydantic.stocks import StockCreate,Stock,StockQueueCreate,StockQueue
# crud, models, schemas
from app.providers import SessionLocal, engine,session,get_db
from datetime import datetime
import logging 
router = APIRouter()
#----------------------------------------------------------------------------------
# STOCKS
#----------------------------------------------------------------------------------


@router.post("/", response_model=Stock,status_code=status.HTTP_201_CREATED)
def create_stock(stock: StockCreate, db: Session = Depends(get_db)):
    db_stock = crud.get_stock_by_id(db, id=stock.id)
    if db_stock:
        raise HTTPException(status_code=400, detail="id already registered")
    return crud.create_stock(db=db, stock=stock)

@router.post("/queue/", response_model=StockQueue,status_code = status.HTTP_201_CREATED)
def create_queue_for_stock( queue: StockQueueCreate, db: Session = Depends(get_db)):
    return crud.create_stock_queue(db=db, queue=queue)

@router.get("/{stock_id}", response_model=Stock,status_code = status.HTTP_200_OK)
def read_stock(stock_id: str, db: Session = Depends(get_db)):
    db_stock = crud.get_stock(db, stock_id=stock_id)
    if db_stock is None:
        raise HTTPException(status_code=404, detail="Stock not found")
    return db_stock
