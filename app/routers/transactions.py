from fastapi import Depends, FastAPI, HTTPException, Request, Response, status, APIRouter
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse

from sqlalchemy.orm import Session

from app.actions import transaction_actions
from app.models.pydantic.transactions import TransactionCreate,Transaction
from app.providers import SessionLocal, engine,session,get_db
from datetime import datetime
import logging 
router = APIRouter()

#----------------------------------------------------------------------------------
# TRANSACTIONS
#----------------------------------------------------------------------------------


@router.post("/deposit", response_model=Transaction,status_code=status.HTTP_202_ACCEPTED)
def create_deposit_transaction_for_user(
    transaction: TransactionCreate, db: Session = Depends(get_db)
):
    return transaction_actions.deposit(db=db, transaction=transaction)


    
@router.post("/withdraw", response_model=Transaction,status_code=status.HTTP_202_ACCEPTED)
def create_withdraw_transaction_for_user(
    transaction: TransactionCreate, db: Session = Depends(get_db)
):
    return transaction_actions.withdraw(db=db, transaction=transaction)
