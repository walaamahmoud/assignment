from typing import List

from fastapi import Depends, FastAPI, HTTPException, Request, Response, status, APIRouter
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse

from sqlalchemy.orm import Session

from app.actions import user_actions
from app.models.pydantic.users import UserCreate,User
from app.providers import SessionLocal, engine,session,get_db
from datetime import datetime
import logging 
router = APIRouter()
#-----------------------------------------------------------------------------------
#  USER
#-----------------------------------------------------------------------------------
@router.post("/", response_model=User,status_code=status.HTTP_201_CREATED)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = user_actions.get_user_by_email(db, email=user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    return user_actions.create_user(db=db, user=user)

@router.get("/", response_model=List[User],status_code = status.HTTP_200_OK)
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = user_actions.get_users(db, skip=skip, limit=limit)
    return users


@router.get("/{user_id}", response_model=User,status_code=status.HTTP_200_OK)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = user_actions.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user
