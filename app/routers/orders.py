from typing import List

from fastapi import Depends, FastAPI, HTTPException, Request, Response, status, APIRouter
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse

from sqlalchemy.orm import Session

from app.actions import order_actions,user_actions,stock_actions
from app.models.pydantic.orders import OrderCreate,Order
# order_actions, models, schemas
from app.providers import SessionLocal, engine,session,get_db
from datetime import datetime
import logging 
router = APIRouter()



#------------------------------------------------------------------------------------
#   ORDER
#------------------------------------------------------------------------------------
@router.post("/buy",response_model=Order,status_code =status.HTTP_201_CREATED)
def create_buy_order_for_user(order:OrderCreate,db:Session=Depends(get_db)):
    
    return order_actions.buy_stock(order=order,db=db)
   

@router.post("/sell",response_model=Order,status_code= status.HTTP_201_CREATED)
def create_sell_order_for_user(order:OrderCreate,db:Session=Depends(get_db)):
    return order_actions.sell_stock(order = order,db=db)
   