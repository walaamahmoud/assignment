from sqlalchemy import create_engine,MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
SQLALCHEMY_DATABASE_URL = "postgres://xcknwruypoybgh:4db6388f5b924e373674417e06158a088bffeb4c8ca79b95ad4ab4e77720319e@ec2-3-208-50-226.compute-1.amazonaws.com:5432/dfa9skeh6bdvvj"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL#, connect_args={"check_same_thread": False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
metadata = MetaData()
session = SessionLocal()