FROM python:3.7
RUN pip3 install fastapi uvicorn
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt 
# CMD ["uvicorn","app.main:app","--host","0.0.0.0","--port","15400"]
COPY . /app
WORKDIR /app
RUN export PYTHONPATH=.

# RUN export PYTHONUNBUFFERED=TRUE
RUN pwd
# CMD ["python3","app/main.py"]
# CMD ["uvicorn","app.main:app","--reload"]
CMD ["uvicorn","app.main:app","--reload","--host","0.0.0.0","--port","8000"]
# uvicorn app.main:notif_app --reload --host 0.0.0.0 --port ${APPLICATION_PORT:-8000}