SQLAlchemy==1.3.19
psycopg2==2.8.5
paho-mqtt==1.5.0	
databases==0.3.2
asyncpg==0.20.1
alembic== 1.4.2
python-dotenv==0.14.0
requests==2.24.0
pytest==6.0.1
